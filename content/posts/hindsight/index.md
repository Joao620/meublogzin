---
title: "Hindsight"
date: 2025-01-09
description: 'Meu fork de um painel de retrospectivas simple e direto'
tags: [sites, react, javascript]
---

![print do site](./Screenshot-Hindsight.png)

Esse projeto é meio diferente de outros meus. Primeiro que ele foi baseado em outro projeto código aberto, segundo que ele é um bocado mais sério. Meu objetivo inicial era só hospedar ele mesmo (para estudar hospedagens), mas acabou que eu precisei mexer bastante nele, e aprender mais um pouco de React 😭

O projeto é o [Hindsight](https://hindsight-for.team/#/). Ele é uma boards de retrospectiva de equipes ágil. Aqueles lances de final de sprint com 'positivos, negativos, melhorias'
# Como começou
Eu já conheci ele de uma maneira legal, que foi procurando um painel desses para hospedar para minha própria equipe. E foi bem difícil achar um. Ou era um negócio bem antigo e não mantido, ou um monstro estupidamente complicado que eu não estava afim de pôr as mãos. 

Dai eu encontrei esse projeto do **haggen**, que era até recente na época que eu o achei, e super simples. Usando meio que só 2 dependências grandes, além de React + Vite. 
E o **haggen** é brasileiro 🇧🇷🇧🇷🇧🇷. Não é muito óbvio, só olhando as redes sociais dele para descobrir, mas ainda é engraçado saber disso
# Hospedagem
No começo mesmo a ideia era hospedar nos planos gratuitos na **GCP**, que eu tinha começado a estudar. Mas durante meus estudos, a **DIO** teve meio que un intensivão da Azure (que foi péssimo. Em geral a **DIO** é péssima. Tutoriais de yt \>\>\>), que me deu e idéia de aproveitar e fazer lá então. Mas eu começei a fazer outra coisa menor lá, achei um saco, e me lembrei das complexidades de usar uma cloud grande assim

Eu já estava querendo fazer de um jeito bem ✨*serverless*✨, porque que a biblioteca é bem flexível, eu poderia inventar bastante moda na parte de esperar o servidor subir para ter a sincronização, mas a aplicação já ficar usável antes e tals. E ao mesmo tempo, eu tava com créditos do **GitHub** *student* para a **Heroku** prestes a expirar. E vendo que ela é exatamente o que eu preciso, decidi deixar de lado isso de big cloud complicada. E é muito estranho a **Heroku** conseguir subir a aplicação em ~10 segundos. Pura magia negra

E o *frontend* tá no **Github** *pages*, firme e forte. Eu particulamente fujo de lá. Já tô 90% migrado para o **Gitlab** (Mas gostei muito do **Codeberg** também!!). Meu objetivo inicial era gastar dinheiro da **Azure** na hospedagem do projeto, mas como eu não consegui, pelo menos da **Microsoft** com a parte dos arquivos no **Github** eu vou. E também é bom que ele fica marcado como `fork` do projeto original, que também está lá.

# Bugs
Isso me deu umas dores de cabeeeeeça.
O principal era um negócio de, ao sair de uma board e ir para um nova, os dados da antiga passavam para essa nova. E isso tem haver com o jeito que a biblioteca para sincronizar os dados estava sendo usada. Eu até poderia fazer uma solução mais gambiarrenta, mas eu aproveitei o pique e re-escrevi bastante parte do código para uma estrutura mais declarativa e integrada com o React, que também resolveria o problema. E foi bom também para conhecer mais do código

E teve outro engraçado. Um valor era sobrescrito para o padrão quando outra pessoa entrava no App. Depois de eu *debbugar* bastante e decidir que o problema não era meu código, eu fui buscar alguém com esses problemas nas issues do projeto. Eu já estava preparando um exemplo para mostrar no código quando eu encontrei uma pessoa falando desse bug. A pessoa sendo **haggen**, autor original do **Hindsight** 😅. Eu achei incrível essa coincidência. Me lembrou de uma parte do código que tinha um gambiarra e falava que era para resolver algum bug. Quase certeza que descobri o bug em questão
# Mudanças
O maior deve ser no servidor, que agora tem persistência em um **Postgres**, melhoria na contagem de participantes, e alguns logs lá no **Datadog**. Fora isso são algumas coisas bobas, de mudanças de texto e tudo mais.

Infelizmente tudo isso está apenas no meu fork. Eu não consegui contato com o **haggen**, e o projeto me parece estar parado por parte dele. Já está a uns meses sem movimentação, e o site fora do ar. Mas quem sabe no futuro eu consiga fazer uns *pull requests* das correções de bugs pelo menos

Tem outras coisas que eu gostaria de adicionar, mas eu não quero arrastar muito mais ele. É um projeto relativamente grande meu, mas eu preciso definir um final também. Dai eu criei várias issues lá na repositório e marcados como `good-first-issues`. Nem todas são tão `good` assim, mas eu gostei da ideia de deixar umas ideias no ar para ter um caminho mais claro para colaborações
# Finalização
Eu quero colocar uma certa publicidade nele, postando em bastante lugares e tudo mais. Até porque ele tá um projeto bem polido, e eu consigo achar um publico maior pra ele. Ele não depende de outras pessoas usarem para ficar útil, por exemplo

# Links dump
Lé sité: https://hindsight-for.team/	\
(Cara, como eu amo esse domínio)

Meu código frontend: https://github.com/Joao620/hindsight-frontend \
Meu código backend: https://github.com/Joao620/hindsight-backend

haggen frontend: https://github.com/haggen/hindsight \
haggen backend: https://github.com/haggen/tinysync


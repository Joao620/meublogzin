---
title: "minha derrota com youtube frontend no termux"
date: 2024-06-07
description: 'explicando um pouco da minha luta com o termux, e já falar de um deles'
---
Não sei exatamente em qual espectro de nerdice eu estou, mas eu faço parte desse de querer ter um servidor em casa. O problema é nunca ter visto utilidade. Eu gosto de hospedar site estáticos, mas existem milhares de servicos para fazer isso gratuito. Nunca gostei de backend, então sem necessidade de um servidor de node ou afins. E backup de arquivos consigo viver fazendo em algum drive aí

Mas uma coisa me ganhou. Um frontend para o youtube. Eu peguei um notebook velho meu e coloquei na minha sala, e é o meu "mídia center". A TV até é smart, mas se a dez anos atrás já era lenta, hoje é insuportável. Ela meio que só servia para espelhar a tela do celular, e a qualidade era destruida pela compressão. Então ele fica lá, para eu assistir alguma coisa no meu almoço.

## Lé problem
 Ele é muito lento ~~(Não me canso de guardar coisa velha em casa)~~. Então para acessar o youtube - que a google faz um belissímo trabalho de encher de tracking e anuncios - força muito a barra do meu celeronzinho. Dá para assitir um vídeo, mas chorando muito para conseguir 720p, e com a navegação no site absolutamente horrosa. E então eu aproveitei para em prática meu servidor

## O programa

Então, o mais conhecido é sem dúvidas o [Inviduos](invidious.io), que parecia ter suporte tranquilo para termux, mas eu encontrei meu primeiro problema. Eu tô usando um lg k10, talvez o 'novo', mas não sei dizer com certeza. Só sei que ele está todo podre, com a bateria segurando dois segundos de carga, a porta USB impossível de usar, botões ruins. Então foi o candidato perfeito para eu soldar fios direto para o carregamento, um botão externo para ligar, e vida que segue. Também teve a vantagem de ser facilmente rooteado, mas no final eu não use nenhum programa que precisasse, e pelas minhas pesquisas é bem desnecessário. Mas ele não tem um processador _aarch64_, mas um _armv7l_. Até onde eu entendi a maior diferença é o primeiro ser 64 bits, em quanto o meu é 32.

Em quanto em questão de performance eu não ligasse muito, o inviduos só é disponível para 64, então nem pensei em tentar futucar isso, e só fui procurar outros frontends mesmo. Seguindo a seguinte lista de [frontend alternativos](https://github.com/mendel5/alternative-front-ends), temos o [Poketube](https://github.com/ashley0143/poketube) e o [cloudtube](https://git.sr.ht/~cadence/cloudtube). Ambos são escritos em javascript, que no termux é suportado tranquilo nodejs, mesmo no meu processador

Mas não foi mesmo tão fácil quando queria

## better-sqlite3

Pensei que seria só um `npm i`, baixar todas outras dependências em js, e vida que segue. Mas a vida não seguiu... Uma das dependências era o tal do `better-sqlite3`, que precisava da compilação do sqlite3, e parece que bindings para ele, não conseguia ser instalada. Uma belíssima mensagem bem criptica de
```
gyp ERR! build error 
gyp ERR! stack Error: `make` failed with exit code: 2
gyp ERR! stack at ChildProcess.<anonymous> (/data/data/com.termux/files/usr/lib/node_modules/npm/node_modules/node-gyp/lib/build.js:209:23)
gyp ERR! System Linux 3.18.35
gyp ERR! command "/data/data/com.termux/files/usr/bin/node" "/data/data/com.termux/files/usr/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js" "rebuild" "--release"
gyp ERR! cwd /data/data/com.termux/files/home/repos/post/cloudtube/node_modules/better-sqlite3
gyp ERR! node -v v20.11.1
gyp ERR! node-gyp -v v10.0.1
gyp ERR! not ok 
npm ERR! code 1
npm ERR! path /data/data/com.termux/files/home/repos/post/cloudtube/node_modules/better-sqlite3
npm ERR! command failed
npm ERR! command sh -c prebuild-install || npm run build-release
```

Na hora eu jogo a mensagem no google, e parece que só precisa passar a opção `--build-deps`. Eu passo a flag, não funciona. Então penso meio que instalar ela globalmente em outra pasta direto do código fonte, que dai ele pode pegar ela já buildada. Clono a lib, npm i, e com algumas futucagens ele builda! Agora é a parte facil de usar ela na outra pasta. . . Eu não consigo usar ela na outra pasta. Tento um tal de comando `npm link`, não tenho ideia de como funciona, e não consegui usar. E sempre que eu preciso fazer alguma coisa que tenta re-instalar a biblioteca, eu tenho que recompilar ela, que em um computador normal seria tranquilo. Leva um belo minuto no pobre do celular. Então já bem puto com isso, decidi sair o Poketube, e tentar o cloudtube...

O cloudtube também dependia dessa biblioteca. Então lá fui eu quebrar mais cabeça com essa desgramada. E cortando muito a emoção da história, a solução foi forçar a usar a versão mais recente dela fazendo um `npm i better-sqlite3@mais-recente-aqui`. E a benevolência do ecosistema do javascript que quebra compatibilidade a cada minor, me permitiu usar ela sem problemas* (o pokefun parece que ainda quebra, mas dessa vez pelo sistema de permissoẽs do termux mesmo).

## Yayyyy sucesso 🥳
Foi o que eu pensei... Depois de usar um pouco, recebi um erro dizendo que a instância do inviduos estava ofline?! Eu não instalei inviduos, de onde vem isso?? Então eu descobrir, depois de lutar muito com esse app, que ele era só um frontend mais fru fru para o inviduos. E se é pra usar o inviduos de outra pessoa, eu uso o frontend original sem dor de cabeça.

Com essa derrota, eu pesquisei sobre cliente linux do youtube. Ainda tive um pouco de dificuldade por estar testando o void linux nele, mas no final fiquei com o [pipe-viwer](https://github.com/trizen/pipe-viewer). Ele tem uma versão com interface, mas eu tô usando a de terminal por ter dificuldades em por a UI pra funcionar. E funciona bem de mais para me convencer a voltar ao termux. 

Esse post não é sobre ele ser ruim. Era só em escolher outro celular dos meus velhos de servidor, instalar inviduos que daria tudo certo. E mais sobre contar sobre essa minha side quest, por que eu gosto de histórias de problemas. Erros são bem legais até, se eu tivesse conseguido sem problemas nenhum eu não teria aprendido o que aprendi. 

Mas sobre a luta que eu tive com o npm era algo que eu poderia ter passado sem aprender. Nossa como eu quebrei a cabeça

---
title: "Meu linkzinho"
date: 2024-06-29
description: 'Um encurtador de link escrito em fatscript'
tags: [fatscript, linkzinho]
tabnews: hagaka/meu-linkzinho-um-encurtador-de-link-escrito-em-fatscript
---

> Cara, eu preciso melhorar os nomes que dou aos meus projetos

Bem, já vou tirar o elefante da sala, fatscript. [Fatscript](https://fatscript.org/) é uma linguagem de programação feita por um brasileiro. Eu vejo ela como uma _toy language_. Isso é, não necessáriamente uma lang que vai mudar o mundo e tudo mais, mas vai servir de estudos e/ou diversão. E eu acho que esse projeto trouxe muito disso. Lá no repositório no GitLab é super organizado, versão semântica (que recentemente chegou no v3!), changelog com as mudanças bem detalhadas, builds automatizadas, docs. É um projeto muito legal de ver sendo desenvolvido, ainda mais sendo por uma pessoa só!

## De onde surgui a ideia
Por isso, e por gostar um bocado de aprender linguagens diferentes, eu sempre quis fazer alguma coisa com ela. A primeira coisa mesmo que eu tentei fazer foi um parser de html. Mas eu compliquei muito, tentei adicionar como um modulo ao interpretador, escrito em C e tudo. No final estava quase funcional, mas nem perto de bom para eu chegar e tentar fazer um merge request ao projeto. Então depois, bem naturalmente me veio esse ideia de um encurtador de links.

## Encurtadores de links
Eu vou passar longe desses mais comuns, que vão estar cheios de tracking, pedir cookies, aceitar termos. eu só preciso de algo simples para uso pessoal. E eu já tinha feito um só para estudos mesmo em Go, mas a oportunidade de um projeto legal com fatscript era boa demais para deixar passar. Pelo menos eu já teria uma base do que queria fazer, e terias alguns códigos para poder traduzir


## Como foi
Muito legal!! (mas leve em consideração minha parcialidade nisso :P). A linguagem tem um syntax diferente, que em si só levam mais viagens até a documentação. O que quebra mesmo a cabeça são os designs da linguagem mesmo, como: early return, cases e switchs, escopo de funções. Então eu sempre fiquei com o repositório do interpretador na máquina, e quando precisava de inspiração de como fazer algo, iria ou na pasta de testes unitários de várias funcionalidades (mais de 100 arquivos!). Ou na pasta de _sample_, que tinha códigos maiores, e até uns joguinhos de terminal! Então fora a minha procrastinação, não houve muitas dificuldades.

## E sobre o site
Originalmente ele foi feito com o boostrap studio. Tenho por causa do [github student](https://github.com/edu/students), e funcionava, mas o peso de todo booststrap + js dele, pra uma página tão simples, não me fez querer levar ele pra frente. E que bom que eu decidi isso porque em uma hora só eu consegui fazer uma nova que não atacava meus nervos (jurava que eu era pior com html e css). E pra levar isso ao extremo, o site não possui javascript, e o também não importa nenhum CSS, só uma tag _style_ dentro da página e nada mais

![front page do site meu linkzinho](./linkzinho.png)

Tem uma funcionalidade ainda que eu queria colocar, da url final poder ser setada apartir de um parametro de url na página. Você então poderia salvar um bookmark com o destino já colocado. E ter um outro bookmark para abrir o link que for encurtado. Mas eu tô mais afim de começar a usar ele logo e ver se isso seria util mesmo, ou é só firula minha.


E mesmo que não é meu plano deixar ele publico para sempre, ele vai tá online por uma ou duas semanas no link [linkzinho.hagaka.me](http://linkzinho.hagaka.me). Mesmo que ele não consuma quase nada de recursos, eu não tô tendo uma VPS minha. E não vai ser só ele que vai me fazer começar a ter uma. E até pela natureza do projeto, se fosse pra publicar assim eu gostaria mais de pegar uma plataforma como a heroku para hospedar, com aquele esquema de parar quando ninguém tivesse usando. Cinco segundos de startup dele não mataria ninguém.

Infelizmente a heroku não tem mais o plano free, mas eu achei alternativas interessantes como o [adaptable.io](https://adaptable.io) e a [qoddi](https://qoddi.com). Só não coloco lá direto porque a db que é um arquivo json em disco, e essas plataformas que são efêmeras não suportam gravar dados em discos desse jeito. Mas se eu ver que tem acessos no projeos e que alguns candangos gostem de usar, eu poderia mudar a db pra usar um mongo da plataforma e deixar lá indefinidamente.

Então esse é a minha jornada com fatscript! Eu pretendo fazer mais posts com ela ainda, dessa vez explicando e mostrando ela mais. Esse daqui ela precisava dividir o espaço com o Linkzinho. Esse post também está no meu [blog](https://weblog.hagaka.me), então fiquem a vontade para assinar o feed Atom/RSS de lá se quiserem ver mais artes minhas.

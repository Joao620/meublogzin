---
title: "Final do meu projetinho"
date: 2024-03-25
description: 'Um lugar para compartilha os nossos queridos projetinhos!'
tags: [sites, meu_projetinho, lua, bootstrap]
#tabnews: hagaka/meu-projetinho-um-lugar-para-compartilhar-nossos-projetos
---
> Ignore não ter tido mais dev vlogs, sou péssimo em dividir algo em pedacinos para conseguir postar

# Meu projetinho

Finalmeeeeente eu terminei esse carinha.
Tudo começou em um [post](https://www.tabnews.com.br/hagaka/onde-encontrar-projetinhos-para-colaborar) meu lá no Tabnews. Eu perguntava se alguém conhecia uma comunidade para encontrar projetos open source brasileiros para colaborar. Ninguém conhecia uma, e tinha gente naquele post compartilhando seus projetos até, mostrando que tinha essa vontade.

Então, como bom arteiro e programador que eu sou, decidi fazer o meu próprio, que demoraria algumas semanas ou um mês no máximo

![](https://i.imgflip.com/85pc8o.jpg)
(isso durou uns 5 meses)

O resultado final ficou bem satisfatório. Mesmo tendo que cortar algumas coisas, e ter que me segurar para não adicionar mais bobeirinhas. O projeto tá aí, tá lançado, e tá pronto.

A stack é extremamente simples (a ideia desde o começo era simplicidade em tudo), é um site estático, com um pré processador gerando as páginas com os projetos e tals

Eu tive a decisão meio duvidosa de usar o bootstrap studio, porque odeio ao máximo desenvolvimento web, e estou muito por fora de padrões, boas práticas, o que precisa colocar no cabeçalho para fazer a bagaça funcionar em mobile. Mas no final foi bem útil, mesmo que precisei me virar muito para me adaptar com ele, e me deu o que precisava

Difícil foi processar os dados dele. Para maior flexibilidade, eu usei lua puro para fazer isso. ficaria difícil usar um pré-processador como hugo porque o bootstrap studio me limitaria muito na questão de criar os templates. Então usei lua, e uma biblioteca para fazer o _parse_ do HTML, e tudo fluiu muito bem

A hospedagem tá sendo feito no GitLab pages, e com o domínio deles também. Se o projeto pegar mais tração eu posso comprar um domínio .com.br, e usar uma CDN melhorzinha. Mas eu quero bota ele pra fora esse MVP logo, e ficar escovando bits depois se precisar.

Em geral tô bem feliz de ter programado um negócio publico, para outras pessoas usarem. Não ser um desafiozinho bobo, que dura uma semana só.

O site é: **joao620.gitlab.io/meu-projetinho**. lá tem algumas informações como adicionar os seus, os meus projetos para demonstrar como fica, e bastantes bugs que eu fingir não existir para lançar ele logo :P
(Usem no modo escuro, eu acho bem mais bonito)

![](https://a.imagem.app/38e4RN.png)

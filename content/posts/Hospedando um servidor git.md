---
title: "Como foi hospedar um servidor Git"
date: 2022-11-27
description: 'Vou compartilhar um pouco de como foi hospedar na Azure uma instância do Gitea'
tags: [selfhost, cloudo]
tabnews: hagaka/como-foi-hospedar-um-servidor-git
---


# Como foi hospedar um servidor git

Essa era uma ideia antiga minhas, por gostar do ambiente open source, querer evitar grandes corporações, e ser arteiro em geral 😅

## O que eu hospedei

Eu hospedei um serviço chamado [Gitea](link aqui), pelos motivos

* **traduzido em português:** o que ajuda em usar ele como portifólio no nosso brasil
* **Similaridade com o Github:** para melhorar a ultilização e adaptação de quem já usa o Github
* **Facilidade para hospedar:** Esse foi um dos maiores motivos, seria meu primeiro _self host_, então queria começar com algo mais fácil

## Como eu hospedei

Usei a azure, não porque eu gosto dela, mas porque ela estava me dando coisas de [graça](https://azure.microsoft.com/en-in/pricing/free-services/)

Estou usando uma VM que eles dão por 12 meses depois de criar uma conta, mas já ouvi umas conversas que o google estava oferecendo uma VM mais simples mas que seria para sempre, que também serveria, ou a oracle que oferece um mostro de 4 nucleos e 24 GB de ram - _mesmo sendo um processador ARM_. Então opção não falta

Tinha tentado fazer a instalação na unha, mas falhei miseravelmente, então agora estou hospedando usando docker, que é ridiculamente fácil.

A VM possui 1GB. No painel do gitea ele mostra um uso de ~100MB. E a VM no todo está consumindo 500MB, juntando o consumo do Caddy, que é o proxy reverso e fornecedor de ssl que eu estou usando.

## Considerações finais

Foi uma experiência muito legal, não posso esperar para hospedar mais coisas, porque idéia não falta

Se quiserem dar uma olhada e experimentar, ele esta hospedado em [git.hagaka.tech](git.hagaka.tech), só não me hackeiem, PRFV ;). Mesmo que o objetivo do repositório é eu ter meu espaço, podem usar se quiserem também. Só não confiem 100% em mim, use senhas fortes, façam backup do código em outro lugar, e sejam consientes em geral
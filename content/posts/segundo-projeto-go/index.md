---
title: "Meu segundo protejo em go"
date: 2024-01-23
description: 'O primeiro não foi terminado ainda, então esse pasou na frente aqui do blog'
tags: [go, docker, dio]
---

Post pequeno, bom para não perder o ritimo do blog

# O plano

Estou estudando docker e go (preciso logo migrar para carreira de cloud). E no curso de docker foi pedido um projeto de servidor web com docker e docker-composer

Lá foi dado um exemplo de servidor estático simples usando apache server, mas tinha um desafio bonus se fosse algo mais elaborado, com servidor e banco de dados. Então eu fiz isso :P

# A aplicação

É um redirecionador de link. É uma coisa que eu já estava pensando em fazer para uso pessoal, então aproveitei a oportunidade. Ela é o básico do básico, faltando ainda uma funcionalidade de link personalizado. Mas esse era um projeto que eu queria muito terminar em um final de semana

Ela está toda feita em go. Eu estava pensando em usar o apache para servir o conteudo estático, o go como backend apenas, e um banco de dados NoSql externo. Mas a facilidade de servir arquivos estáticos em go, e a dificuldade de um banco simples e leve, me levou a fazer tudo em go mesmo

Então está sendo usado a biblioteca padrão do go para servir o arquivos e API, e a biblioteca pogreb, que é praticamente um map com persistência no disco

# Resultado

![Página inicial, contendo o titulo, um input para o link, e um botão para encurtar](imagem-redirector.png)

Estou orgulho para o que me levou apenas uns minutos, e alguns goles de café. Eu ainda vou fazer um dropdown para opcionalmente selecionar um link, e algumas validações de erros também, porque agora tá bem fácil colocar um link inválido e receber um 404 ou 501

---
title: "Meu projetinho Devlog 1"
date: 2023-11-19
description: 'Um lugar para compartilha os nossos queridos projetinhos!'
tags: [sites, meu_projetinho]
#tabnews: hagaka/como-foi-hospedar-um-servidor-git
---

## Problema:
É dificil ter uma comunidade em volta de pequenos projetos de código aberto. Você pode caçar em servidores git por projetos, mas não acho que tenha a melhor experiência fazendo desse jeito.

Acho que assim vai se achar mais projetos grandes, que não tem problema você querer contribuir, mas acho meio assustador para um novado em open source, uma alta barreira de aprendizado do código, dificuldade de comunicação. Em fim, acho que deu para entender a ideia.

E se não for isso, vão ter muitos projetos meio que só para testes, pessoais, que não tem quere ir mais para frente, ou aceitar contribuição

## Proposta: 
Um lugar que o foco é a compartilhação de projetos. Com o intuito de tanto você poder ver projetos brasileiros, e contribuir se gostar dele. Tanto para você ter um lugar para compartilhar seus próprios projetos

## Vou conseguir?:
Seila, tenho que tentar né. Pelo menos seria muita ironia abandonar esse projeto, meio contra a ideia dele

---

Tá, agora parando de listinhas, isso é uma coisa que eu procurei e não achei. Sinto um pouco de solidão nesse négocio de projetinhos, e não acho que precisa ser assim. Poderia ter muito mais um senso de comunidade. Contigo tendo um lugar para compartilhar suas vitórias, receber feedback de pessoas usando ou contribuindo, e você poder se conectar com uma pessoa tão nerd quanto você, fazendo algo que os dois gostam.

Eu simplismente adoro poder pegar uma ideia doida e poder desenvolver algo para isso. Mas mesmo a ideia sendo legal, não tem como se focar em terminar algo mais longo que um mês, quando o resultado final é o mesmo: Você, fazendo algo sozinho, que possívelmente não tem publico, e que talvez ninguém além de você vai ver/usar.

Então minha idéia é tentar criar um lugar para mudar isso. Pelo menos para isso eu tenho uma motivação incrível para fazer

Vendo que já tá bem longo esse blog, vou tentar ser breve com o estado do projeto, nos próximos devlogs vou detalhando mais

Ele tá usando uma estrutura o mais simple que consigo, tirando a parte do design, que estou usando o bootstrap studio (obrigado github student). Até agora ele serve bem, me ajuda bastante já que odeio web design, e não conheço quase nada de bons hábitos e padrões. Ele lida com isso tudo. E o site todo vai ser estático, sem banco de dados, contas, nada. Vai ser um repositório git, que vai aceitar contribuições para adicionar projetos. E uma ferramenta escrita em Lua para transformar o html gerado pelo bootstrap, mais os dados de todos os projetos adicionados, no site. Site que vai ser hospedado pelo próprio servidor git, e agora está sendo o GitLab, que pretendo continuar usando.

E o estado atual é: O design tá meio que pronto, já tive bastante dificuldade em fazer o que tem, não quero ter que quebrar mais a cabeça com ele. E o script de geração do site está sendo feito, ele tá com uma estrutura meio boa, mas está bem no inicio ainda

![listagem dos projetos, com 3 itens de placeholder](lista-projetos.png)
![a página que teria um projeto, mas com textos de placeholder](projeto-sample.png)

E fora isso, ainda vou precisar fazer o filtro na parte da lista. Isso vai ser um trabalho em conjunto com o lua e o bootstrap. Porque ele vai ter que dá ultilidade no html gerado, ao mesmo tempo usar dados que foram conseguidos pelo o lua. Espero não dar tanto trabalho quando parece que vai dar.

E é isso, só um primeiro post para quebrar o gelo, vamos ver como e aonde isso vai dar

---
title: "levando meu blog ao mastodon"
date: 2024-09-03
description: 'Uma provavel séries de posts meus fazendo arte para meu blog ser um perfil no mastodon'
tags: [hugo, azure, blog, mastodon]
---

Essa vai ser uma séries de post de mim fazendo uma "conta" no mastodon no meu blog com a infraestrutura da azure


## Tirando o elefante da mesa

*trocadilho intencional*


Mastodon é uma rede social descentralizada, isto é, existem vários servidores diferentes, mas todos eles podem se comunicar entre si. Então eu que estou no [bolha.us](https://bolha.us/), posso seguir pessoas do [mastodon.social](https://mastodon.social/), e interagir da mesma maneira de se eles estivessem na minha instância!

Não só isso, mas eu posso interagir também com pessoas de outros aplicativos que também utilizam o *ActivityPub*. Daí talvez entre o conhecimento mais geral das pessoas, porque o [Threads](https://www.threads.net) e o [Bluesky](https://bsky.app/) também implementam esse protocolo. Logo, eu no bolha.us, posso seguir você no Threads


## Porque levar meu blog pra aí??


Para mim, a maior vantagem é ter um sistema de comentários no meu blog, sem depender de um aplicativo de terceiros. E também tem o grande ponto de permitir pessoas a seguirem o seu blog, e receberão notificações de postagem, e até aparição na sua timeline!


Isso pode ser implementado de uma maneira mais simples, usando serviços como o [MastoFeed](https://mastofeed.org/), que pegaria o feed Atom/RSS que seu blog já tenha, e espelharia ele em uma conta bot no mastodon, mas essa implementação tem 3 problemas para mim


* Não tem a parte da arte e dos estudo 🤪

* Os dados não ficam realmente comigo, ficam no serviço deles

* Os comentários não seriam colocados de forma estática no HTML, que eu quero


## Como??


Antes de ficar muito tarde, eu vou basear bastante do meu trabalho no [nesse cara aqui](https://maho.dev/2024/02/a-guide-to-implement-activitypub-in-a-static-site-or-any-website/), que ele fez isso, mas que eu vou querer/precisar modificar


Primeiro é a questão das ferramentas utilizadas, que talvez pelo conhecimentos prévios dele, ele fez umas escolhas que eu quero mudar


### hospedagem do site com azure blob storage

A alternativa que eu quero usar é o Aplicativos Web Estáticos, com ela eu consigo ter domínios customizados mais facilmente, e build do meu blog direta com CI/CD


### Comentários de forma estática no HTML

Agora que eu tenho uma forma de buildar o blog de maneira fácil, eu quero poder fazer que a cada comentário colocado, ele seja buildado ali rapidinho, para não precisar criar APIs, e nem precisar ter JS na página para carregar nenhum comentário


### Azure Table Storage como DB

É meio difícil dizer a diferença entre essa Table Storage e a Azure CosmosDB. Mas a principal para mim é que a CosmosDB é de graça até um limite de uso por mês. Que pra essa aplicação somente, dá e sobra de mais!



## A stack


No papel, fica mais ou menos assim

![diagrama com todos serviços que pretendo usar](./diagrama.png)


Primeiro diagrama que faço, tô experimentando!


Eu pesquisei bastante sobre as todas ferramentas, e acredito que elas vão servir sim! Mas só testando para saber


Se meu blog fosse maior eu não acho que essa solução escalaria muito bem. Em geral uma ideia de recriar o site é bem ineficiente, e o build ainda está lidando com a notificação no mastodon depois (mesmo que não parece muito difícil mudar para Azure Functions depois se for problemático). Mas de novo, só testando para saber se essa solução vai funcionar, senão eu encontro outra gambiarra para fazer isso funcionar!

Então agora partiu a parte difícil, e codar tudo isso!

É isso, é nois!